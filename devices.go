package main

import("fmt")

//Device implements a device
type Device struct {
	ID   			string `json:"ID"`
	Name 			string `json:"Name"`
	DeviceType 		string `json:"DeviceType"`
	RGBA 			Values `json:"Values"`
}

//Values implements the RGBA values of the device 
type Values struct{
	R    string `json:"R"`
	G    string `json:"G"`
	B    string `json:"B"`
	A    string `json:"A"`
}

//NewDevice creates new Device object
func NewDevice(id, name, t , r, g, b ,a string) *Device {
	return &Device{
		ID: id,
		Name: name,
		DeviceType: t,
		RGBA:Values{
			R:r,
			G:g,
			B:b,
			A:a,
		},
 	}
}

//NewEmptyDevice creates new empty Device object
func NewEmptyDevice() *Device {
	return &Device{
		ID: "",
		Name: "",
		DeviceType: "",
		RGBA:Values{
			R:"",
			G:"",
			B:"",
			A:"",
		},
 	}
}

func (dev *Device) toString()string{
	return fmt.Sprintln("[",dev.ID,dev.Name,dev.DeviceType,"Values:",dev.RGBA.R,dev.RGBA.G,dev.RGBA.B,dev.RGBA.A,"]")
}