package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/handlers"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", Health).Methods("GET")
	router.HandleFunc("/frontend-api/get-device/{id}", GetDevice).Methods("GET")
	router.HandleFunc("/frontend-api/update-rgb", UpdateRGB).Methods("PUT")
	//enable CORS
	log.Fatal(http.ListenAndServe(":80", handlers.CORS(handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}), handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"}), handlers.AllowedOrigins([]string{"*"}))(router)))
}
