package main

import (
	"log"
	"net/http"
	"fmt"
	"io/ioutil"
	"encoding/json"
	"strings"

	"github.com/gorilla/mux"
)


//function which sends msg to client
func sendMessage(w http.ResponseWriter, code int, msg string) {
	w.WriteHeader(code)
	w.Write([]byte(msg))
}

//Health is a healthpoint
func Health(w http.ResponseWriter, r *http.Request){
	log.Println("/ hit")

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("healthy"))
}

//GetDevice returns the given device
func GetDevice(w http.ResponseWriter, r *http.Request){
	log.Println("/frontend-api/get-device/{id} hit")

	HTTPclient := &http.Client{}
	vars := mux.Vars(r)["id"]

	serviceName := "redis-rest:80"
	updateURL := fmt.Sprintf("http://%s/api/get-device/%s", serviceName,vars)

	//create request to agent
	req, err := http.NewRequest("GET", updateURL, nil)
	if err != nil {
		log.Println("[ERROR] Can not create request: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not create request"))
		return
	}
	//send request
	resp , err := HTTPclient.Do(req)
	if err != nil {
		log.Println("[ERROR] Can not connect to Redis-API request: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not connect to Redis-API request"))
		return
	}

	//read device info from client
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("[ERROR] Can not parse client data: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not parse client data"))
		return
	}

	sendMessage(w,http.StatusOK,string(body))
}

//UpdateRGB updates the rgb Values of the given device
func UpdateRGB(w http.ResponseWriter, r *http.Request) {
	dev := NewEmptyDevice()
	HTTPclient := &http.Client{}
	//read device info from client
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("[ERROR] Can not parse client data: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not parse client data"))
		return
	}
	//converting client data to struct
	err = json.Unmarshal(body, &dev)
	if err != nil {
		log.Println("[ERROR] Can not convert JSON to struct: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not convert JSON to struct"))
		return
	}
	//convert device struct to JSON
	devJSON, err := json.Marshal(dev)
	if err != nil {
		log.Println("[ERROR] Can not convert struct to JSON: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not convert struct to JSON"))
		return
	}
	//send JSON to device 
	serviceName := "device-rest:80"
	updateURL := fmt.Sprintf("http://%s/device-api/update-rgb", serviceName)
	//create request to agent
	req, err := http.NewRequest("PUT", updateURL, strings.NewReader(string(devJSON)))
	if err != nil {
		log.Println("[ERROR] Can not create request: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not create request"))
		return
	}
	//send request
	_, err = HTTPclient.Do(req)
	if err != nil {
		log.Println("[ERROR] Can not connect to Redis-API: ",err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("[ERROR] Can not connect to Redis-API"))
		return
	}
}